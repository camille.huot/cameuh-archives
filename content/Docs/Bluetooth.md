---
title: "Gentoo - Notes sur le Bluetooth"
date: 2007-08-23
---

Suivre le [[http://www.gentoo.org/doc/fr/bluetooth-guide.xml|guide Gentoo]] pour faire fonctionner le contrôleur Bluetooth sur le poste client.

## Configuration du noyau

Cocher les modules Bluetooth comme spécifié dans http://www.gentoo.org/doc/fr/bluetooth-guide.xml#doc_chap3

Mettre aussi FUSE.

```
make modules modules_install
```

## Installer les outils

```
emerge net-wireless/bluez-libs net-wireless/bluez-utils obexfs
```


## Brancher la clé USB Blutooth

udev devrait alors charger le service /etc/init.d/bluetooth

## Vérifier que ça marche

### Présence du périphérique Bluetooth

```
hcitool dev
```

### Scan des voisins

```
hcitool scan
```

## Mount du téléphone

(les répertoires visibles se configurent sur le téléphone)

```
sudo mkdir /media/sam
sudo /etc/init.d/fuse start
sudo mount -t fuse "obexfs#-b00:1A:8A:53:D2:F8" ~/Bluetooth/
```

Rem: ca ne marche pas encore...

# Utiliser obexftp

Quelques petits exemples pour transférer des fichiers vers ou depuis le téléphone.


## Copier une série de MP3 vers le téléphone

```
export MAC=00:1A:8A:53:D2:F8
obexftp -b $MAC -c "/Sounds & Ringtones/User" -p $(ls *mp3)
```

## Script pour récupérer l'ensemble des photos (taguées bluetooth) avec gestion des versions

```
##/bin/bash

MAC=00:1A:8A:53:D2:F8
DEST=/Pictures/User

DATE=$(date +%25Y%25m%25d)
i=1
while # mkdir "$DATE-$(printf %2502d $i)" 2> /dev/null; do
	if [ $i -eq 99 ]; then
		echo "Unable to create a directory to store files."
		exit
	fi
	let i++
done

DEST="$DATE-$(printf %2502d $i)"

echo "Fetching files list (remember to make files visibles)"
XML=$(obexftp -b $MAC -l $DEST)

echo "Fetching files into $DEST"
(cd $DEST; obexftp -b $MAC -c $DEST -g $(echo -e "$XML" | grep file | cut -d'"' -f2 | tr "\n" " "))
```

Il établiera deux connexions avec le téléphone, 1 pour obtenir la liste des fichiers, 1 autre pour les télécharger.

Les fichiers sont placés dans un répertoire contenant la date du jour + une version.
