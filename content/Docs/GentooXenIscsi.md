---
title: "System - Xen sur du stockage Gentoo/iSCSI"
date: 2008-04-29
---

## Configuration :

* 2 serveurs de stockage en iscsi, failover
* 4 serveurs xen

## Installer le serveur iSCSI

Chaque volume à exporter en iSCSI est créé dans un Volume Group LVM, ce qui permet une grande flexibilité.

### Installer LVM

```
pvcreate /dev/cciss/c0d0p2
vgcreate vg /dev/cciss/c0d0p2
```

### Créer un volume LVM pour un partage iSCSI

Nous avons besoin d'un espace de stockage de 10Go pour une machine virtuelle Xen :

```
lvcreate -n r5o11n1v1 -L 10G vg
```

Le nom r5o11n1v1 est inspiré de la future machine virtuelle :
* redhat 5
* oracle 11
* node 1
* volume 1

### Installer la target iscsi

http://gentoo-wiki.com/HOWTO_iscsi pour plus de détails

```
echo "sys-block/iscsitarget" >> /etc/portage/package.keywords
emerge iscsitarget
modprobe iscsi_trgt
```

Configurer le serveur : /etc/ietd.conf

```
Target iqn.2008-03.net.fti:storage.r5o11n1.vol1
 Lun 0 Path=/dev/vg/r5o11n1v1,Type=blockio
```

Le numéro de LUN est à incrémenter s'il y en a plusieurs.

Configurer le service : /etc/conf.d/ietd

Perso, j'ai changé le PORT.

```
/etc/init.d/ietd start
```

Vérifiez les logs, s'il y a un problème, c'est qu'il manque surement un module noyau.

## Installer un serveur Xen

http://www.gentoo.org/doc/fr/xen-guide.xml pour plus d'infos

On utilise un seul noyau pour le dom0 et les domU, plus facile.

Pour les domU, on utilisera si possible le noyau de la distribution cible (ce n'est pas toujours possible : problème de compatibilité entre les versions hôte et cible de Xen...).

## Installer un initiator (client) iSCSI

```
echo sys-block/open-iscsi >> /etc/portage/package.keywords
emerge open-iscsi
mkdir /etc/iscsi
ln -s ../initiatorname.iscsi /etc/iscsi/initiatorname.iscsi
```

Fichier de conf de l'initiator /etc/initiatorname.iscsi :

```
InitiatorName=iqn.2008-03.net.fti:storage.r5o11n1.vol1
InitiatorAlias=vol1
```

Démarrage et connexion :

```
/etc/init.d/iscsid start
iscsiadm -m discovery -t st -p 10.234.78.202:80 -P 1
iscsiadm -m node -T iqn.2008-04.net.fti:storage.gentoo3node1.disk2 -l
```

Vérification :

```
fdisk -l
[...]
Disk /dev/sda: 56.9 GB, 56908316672 bytes
64 heads, 32 sectors/track, 54272 cylinders
Units = cylinders of 2048 * 512 = 1048576 bytes
Disk identifier: 0x00000000
```

Il ne se distingue pas vraiment des autres, mais c'est pourtant bien lui, le périphérique iSCSI #

## Installer un domU Centos 5.1

### Extraire un système Centos 5.1 sur le périphérique iSCSI

Nous allons prendre une image Centos déjà installée chez Jailtime.

Comment utiliser les fichiers fournis par Jailtime : http://jailtime.org/download:start

Prendre le fichier bz2 ici : http://jailtime.org/download:centos:v5.1

On extrait maintenant l'image du système sur le périphérique iscsi (on peut faire ça sur n'importe quel serveur Xen client iScsi, cela revient au même).

/!\ Attention que ce soit bien '''sda''' pour vous, il ne faudrait pas écraser le système hôte ## partitionnez comme vous voulez, mais sda1 (/) doit faire au moins 600Mo.

```
fdisk /dev/sda
mkfs.ext3 /dev/sda1
mkdir /mnt/isda1
mount /dev/sda1 /mnt/isda1

tar xjf centos.5-1.20080125.img.tar.bz2
mkdir /mnt/image
mount -oloop centos.5-1.img /mnt/image

cd /mnt/image
tar cf - * | tar xvf - -C /mnt/isda1/

cd ~
umount /mnt/image
umount /mnt/isda1
```

Nous avons installé Centos5.1 sur notre disque iSCSI. Vous pouvez supprimer tous les fichiers que nous avons téléchargés si vous le souhaitez, nous n'en avons plus besoin.

### Configurer le domaine Xen

Pour créer un domaine, il suffit de renseigner un fichier texte /etc/xen/node1 :

```
kernel = "/boot/kernel-2.6.21-xen"
memory = 512
name = "node1"

disk = [ 'phy:sda,sda,w' ]
root = "/dev/sda1 ro"

vif = [ 'bridge=xenbr0' ]
```

Le noyau est celui que nous utilisons pour notre Gentoo hôte. Normalement il convient.

Nous déclarons un disque, il s'appelle "sda" sur la machine hôte, c'est le périphérique iSCSI, et il s'appellera "sda" également dans la machine virtuelle. Il est monté en écriture.

"root=" indique à Linux de booter sur /dev/sda1.

"vif" permet de créer une interface pontée entre la machine virtuelle et la machine hôte, ainsi nous serons directement sur le réseau.

### Démarrer la machine virtuelle

```
/etc/init.d/xend start
xm list
xm create node1
xm list
xm console node1
```

Si tout se passe bien, vous devriez avoir le prompt Centos. Notez que la carte réseau démarre en DHCP, que le mot de passe root est "password" et que vim n'est pas installé (il y a nano à la place) !

## Liens

*http://www.open-iscsi.org/docs/README
*http://www.gentoo.org/doc/fr/xen-guide.xml
*http://www.cyberciti.biz/tips/rhel-centos-fedora-linux-iscsi-howto.html
*http://wiki.centos.org/HowTos/Xen/InstallingCentOSDomU
*http://gentoo-wiki.com/HOWTO_iscsi
*http://jailtime.org/
*http://anothergeekwebsite.com/fr/2007/06/xen-vlan-et-bonding-oui-oui-tout-ca
*http://www.performancemagic.com/iscsi-xen-howto/
*http://dokuwiki.solstice.nl/dokuwiki/doku.php?id=xen:live-migration_infrastructure
