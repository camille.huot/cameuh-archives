---
title: "System - Configurer un port infrarouge (irda) sous Linux"
date: 2007-08-06
---

# Brouillon

Cette page explique comment faire fonctionner le chipset infrarouge du Thinkpad X40 avec une télécommande de télé et un clavier infrarouge.

Référence :

http://www.thinkwiki.org/wiki/How_to_make_use_of_IrDA

## Installation du contrôleur Irda sur le PC

Activer en '''module''' le driver du contrôleur irda du Thinkpad.

Si j'ai bien compris, pratiquement tous les Thinkpad ont le même contrôleur : NSC-IRCC

Il faut le mettre en module pour pouvoir le configurer en ligne de commande.

Dans Networking :

```
CONFIG_IRDA=m
CONFIG_IRCOMM ?
CONFIG_NSC_FIR=m
```

### Le Bins avec le port série

Par défaut, Linux crée 4 ports séries et les configure tous les 4 automatiquement. Le Thinkpad n'en a qu'1 et nous en avons besoin d'un pour l'IRDA.

Dans Character Device -> Serial :

```
CONFIG_SERIAL_8250_NR_UARTS=4
CONFIG_SERIAL_8250_RUNTIME_UARTS=1
```

Dans Bus (PCI, ISA), le PNP pour que Linux puisse configurer le contrôleur :

```
CONFIG_ISAPNP=y
```

Une fois le noyau recompilé. On profite du reboot (obligatoire si ISAPNP n'était pas activé) pour vérifier les réglages ISA du contrôleur IRDA du BIOS, que l'on réutilise pour configurer les modules, dans /etc/modules.conf :

```
alias irda0 nsc-ircc
options nsc-ircc dongle_id=0x09 io=0x2f8 irq=3 dma=3
install nsc-ircc /bin/setserial /dev/ttyS1 uart none port 0 irq 0; /sbin/modprobe --ignore-install nsc-ircc
```

Où dongle_id, io, irq et dma correspondent aux valeurs définies dans le BIOS.

La ligne install n'est utile que si on n'a pas modifié le nombre de ports série alloué automatiquement par Linux plus haut. Setserial attribue dans ce cas ttyS1 (inutilisé en principe) au port série IRDA.


## Recevoir les signaux de la télécommande
## Binder les signaux à MPlayer
## Recevoir les signaux du clavier
## Binder les signaux à Xorg
