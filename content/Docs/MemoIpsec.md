---
title: "Architecture VPN IPSEC avec certificats, OpenBSD et Linux"
date: 2007-04-27
---

Cet article est un mémo qui trace toutes les manipulations à faire pour obtenir une architecture de VPN Ipsec basée sur un système d'authentification par certificats x509 dans un contexte de serveur OpenBSD et de clients Linux.

Si possible, on abordera les problématiques du DHCP et du WIFI.

# Autorité de certification

Une clé privée est la garante de l'autorisation des différents systèmes à pouvoir se connecter au serveur VPN. Elle permet de signer les certificats des différentes machines à autoriser.

On rangera les différents fichiers dans @@/etc/ssl/@@.

## Création du certificat racine

Le certificat est @@ca.crt@@, il sert à vérifier les certificats de machines (à distribuer sur toutes les machines).

La clé privée est @@ca.key@@, elle servira pour créer les certificats de machines. Il est recommandé de la protéger par un mot de passe fort (lors de la génération).

```# openssl req -x509 -days 365 -newkey rsa:1024 \
                        -keyout /etc/ssl/private/ca.key \
                        -out /etc/ssl/ca.crt
```


# Un serveur Ipsec x509 sous OpenBSD 4.1 avec Isakmpd

## Certificat

On crée la clé privée du serveur VPN, on demande un certificat signé par le CA et on l'installe.

```
# DATE=`date +%25Y%25m%25d`
# openssl genrsa -out /etc/isakmpd/private/local.key 2048
# chmod 400 /etc/isakmpd/private/local.key
# openssl req -new -key /etc/isakmpd/private/local.key -out /etc/ssl/requests/$(hostname)-$DATE.csr
(mettre le FQDN dans le champd CN)
# openssl x509 -req -days 365 -in /etc/ssl/requests/$(hostname)-$DATE.csr \
 -CA /etc/ssl/ca.crt -CAkey /etc/ssl/private/ca.key \
 -CAcreateserial -extfile /etc/ssl/x509v3.cnf \
 -extensions x509v3_FQDN -out /etc/ssl/certs/$(hostname)-$DATE.crt
# cp /etc/ssl/certs/$(hostname)-$DATE.crt /etc/isakmpd/certs/$(hostname).crt
# cp /etc/ssl/ca.crt /etc/isakmpd/ca/
```

## ipsec.conf

Table de routage ipsec :

```
ike passive from 10.4.15.13 to 10.4.15.100 srcid kerberos.cameuh.net dstid x.cameuh.net
```

## isakmpd.policy

## Lancer le bins


```
isakmpd -d -4 -DA=90
This will start the isakmpd in foreground (-d) using IPv4 (-4) and a debuglevel of 90.
```

# Un client Ipsec x509 sous Linux avec Racoon

## Créer un certificat de machine cliente, signé par le CA

Au début, on se trouve sur la machine cliente Linux qui utilisera Racoon.

### La clé privée

On crée une clé privée qui identifiera la machine au travers du client VPN Racoon.

```
# mkdir -p /etc/racoon/certs
# openssl genrsa -out /etc/racoon/certs/local.key 2048
# chmod 400 /etc/racoon/certs/local.key
```

### Une demande de certificat

On crée alors une demande de certificat :

```
# openssl req -new -key /etc/racoon/certs/local.key -out /etc/racoon/certs/$(hostname).csr
```

Répondre le FQDN de la machine (l'identifiant) à la question "Common Name".

Envoyer le fichier @@.csr@@ (la demande de certificat) à l'autorité de certification.

### Création du certificat

Sur le CA, réceptionner la demande en intégrant la date de réception dans le nom du fichier :

```
# CERTFQDN=(mettre le FQDN ici)
# DATE=`date +%25Y%25m%25d`
# mv hostname.csr /etc/ssl/requests/$CERTFQDN-$DATE.csr
```

Créer le certificat correspondant (valide pdt 182 jours) :

```
# openssl x509 -req \
 -days 365 -in /etc/ssl/requests/$CERTFQDN-$DATE.csr \
 -CA /etc/ssl/ca.crt -CAkey /etc/ssl/private/ca.key \
 -CAcreateserial -extfile /etc/ssl/x509v3.cnf \
 -extensions x509v3_FQDN -out /etc/ssl/certs/$CERTFQDN-$DATE.crt
```

Le champ '''subjectAltName''' contient alors l'ID du client.

Retourner le @@.crt@@ et le @@ca.crt@@ au client.

### Ajouter le certificat au trousseau du serveur

'''Non, à quoi sert le CA si on doit manuellement ajouter un fichier au serveur VPN ?'''

```
# cp -i /etc/ssl/certs/$CERTFQDN-$DATE.crt /etc/isakmpd/certs/$CERTFQDN.crt
```



### Mise en place sur la machine client

Placer les certificats (machine + CA) dans @@/etc/racoon/certs/@@.

Un lien spécial doit être créé pour que racoon trouve le certificat du CA :

```
# cd /etc/racoon/certs/
# ln -s ca.crt `openssl x509 -noout -hash %3c ca.crt`.0
```

## Client VPN

On indique à Linux quelles routes doivent utiliser IPsec grâce à @@setkey@@.

Linux, lorsque le besoin se fera sentir, demandera à Racoon d'établir les canaux sécurisés.

Pour résumer :
* Les "tables de routage" de Linux sont configurées via setkey dans @@/etc/ipsec.conf@@.
* Les détails liés à l'établissement de ces routes, c-à-d. la gestion des certificats etc., sont réalisés par Racoon à la demande du noyau.

## Mise en place des règles IPsec dans Linux

Sous Gentoo c'est dans @@/etc/ipsec.conf@@ (voir @@/etc/conf.d/racoon@@).

```
##/usr/sbin/setkey -f

# Nettoyage des SA et de la SPD
flush;
spdflush;

# les SA sont gerees par Racoon

# les policies de la SPD (en gros : les regles de routage)
spdadd 192.168.5.72/32 10.4.0.0/16 any -P out ipsec esp/tunnel/192.168.5.72-81.57.199.217/require;
spdadd 10.4.0.0/16 192.168.5.72/32 any -P in  ipsec esp/tunnel/81.57.199.217-192.168.5.72/require;
```

## Configuration du démon IKE (Racoon)

On fait une configuration générique sans spécifier d'adresses IP. On fait confiance en la vérification du certificat pour la sécurité.

Sous Gentoo (voir @@/etc/conf.d/racoon@@), c'est dans @@/etc/racoon/racoon.conf@@.

```
path certificate "/etc/racoon/certs";

listen {
 isakmp 192.168.5.72 [25252];
 isakmp_natt 192.168.5.72 [25255];
}

remote anonymous {
 exchange_mode main;
 certificate_type x509 "camille.exploitadm.ftmms.crt" "local.key";
 ca_type x509 "ca.crt";
 verify_cert on;
 my_identifier fqdn camille.exploitadm.ftmms;
 nat_traversal on;
 proposal {
  encryption_algorithm 3des;
  hash_algorithm sha1;
  authentication_method rsasig;
  dh_group modp1024;
 }      
}

sainfo anonymous {
 pfs_group modp768;
 encryption_algorithm 3des;
 authentication_algorithm hmac_sha1;
 compression_algorithm deflate;
}
```

## Tester le service

On charge les règles SPD :

```
# setkey -f /etc/ipsec.conf
```

On lance Racoon en mode foreground pour voir ce qu'il se passe :

```
# racoon -F -f /etc/racoon/racoon.conf
```

## Lancer le service sous Gentoo

Modifier la conf du service dans @@/etc/conf.d/racoon@@ :

Commenter la ligne qui définit @@RACOON_PSK_FILE@@ puisque Gentoo vérifie la présence de ce fichier que nous n'utilisons pas.

Puis activer et lancer le service :

```
# rc-update add racoon default
# rc
```

# Un client Ipsec sous Windows avec Cisco VPN Client

# Références

Dans l'ordre :
* [[http://www.openbsd.org/cgi-bin/man.cgi?query=ipsec&sektion=4&arch=i386&apropos=0&manpath=OpenBSD+Current|man ipsec(4)]]
* [[http://www.openbsd.org/cgi-bin/man.cgi?query=isakmpd&sektion=8&arch=i386&manpath=OpenBSD+Current|man isakmpd(8)]]
* [[http://www.openbsd.org/cgi-bin/man.cgi?query=ipsec.conf&sektion=5&arch=i386&apropos=0&manpath=OpenBSD+Current|man ipsec.conf(5)]]
* [[http://www.openbsd.org/cgi-bin/man.cgi?query=isakmpd.policy&sektion=5&arch=i386&apropos=0&manpath=OpenBSD+Current|man isakmpd.policy(5)]]
* [[http://www.fernhilltec.com.au/~adrian/OpenBSD-PKI-IPSEC-HOWTO.html | OpenBSD-based VPN connectivity using PKI: Certificate Generation, Signing and Installation]]
* [[http://www.ipsec-howto.org/x304.html|Linux Kernel 2.6 using KAME-tools]]

Vieux articles :
* http://www.atlantilde.com/docs/OpenBSD-swlan.html
* http://www.klake.org/~jt/tips/80211.html
