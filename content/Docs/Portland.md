---
title: "System - Utiliser les xdg-tools"
date: 2010-11-08
---

Le paquet se nomme xdg-tools mais le projet s'appelle Portland.

XDG est l'ancien nom de Freedesktop.org : X Desktop Group.

Ce paquet fournit des outils "standards" pour lier une extension de fichier à un programme pour les ouvrir.

Par exemple, il va lier l'extension ".pdf" à Acrobat Reader.

Il suffira alors d'ouvrir le fichier avec "xdg-open" et celui-ci va ouvrir /usr/bin/acroread.

Les xdg-tools doivent normalement être utilisés par tous les outils de type navigateur de fichier. C'est le cas de chromium par exemple.

### Trouver le type MIME d'un fichier

```
$ xdg-mime query filetype incredibly-patient-dogs.wmv
application/octet-stream; charset=binary
```

application/octet-stream est le type par défaut. Cela signifie que l'extension n'est pas reconnue.

### Ajouter un type MIME local

Les types MIME '''NE SONT PAS''' définis dans /etc/mime.types et ~/.mime.types. Mais on peut s'en inspirer :

```
$ grep wmv /etc/mime.types ~/.mime.types
/etc/mime.types:video/x-ms-wmv				wmv
```

Pour ajouter un type MIME, il faut créer un petit fichier XML '''ms-wmv.xml''' :

```
<?xml version="1.0"?>
<mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'>
<mime-type type="video/x-ms-wmv">
 <glob pattern="*.wmv"/>
</mime-type>
</mime-info>
```

Et le charger :

```
$ xdg-mime install ms-wmv.xml
```

Cela se passe dans le répertoire ~/.local/share/mime/
