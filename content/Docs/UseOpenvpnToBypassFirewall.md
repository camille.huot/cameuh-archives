---
title: "System - Transparently Bypass Enterprise Firewall using Linux and OpenVPN"
date: 2007-10-26
---

# Use OpenVPN To By-Pass Firewall

The idea behind this article is taken from a particular situation:
* your workstation is firewalled at work
* you only have a few ports open (that is, '''directly''', without Proxy)
* you're using Linux
* you have root privileges
* you have root privileges on a box elsewhere (let's say a xDSL box at home) with Internet access without network restriction

To enable full access from your workstation to Internet, we will use some technologies:
* Linux Netfilter (mangle and MARK)
* OpenVPN
* iproute2

I assume your workstation can use ports 80, 443 and 8080 to Internet.

We don't need to know your local IP address at work.

We don't need to know your local IP address at home.

We need to define a local subnet openvpn will use to allow workstation and homestation to communicate. This subnet should not ever be in use at work or at home. Let's say: 10.4.16.0/24.

The workstation is firewalled. So it will connect to the homestation using an allowed TCP port, assume 443. Port 443 should not be already in use at home. We need the public IP address for homestation, let's say: 100.100.100.100.

## OpenVPN

OpenVPN is a software to make a VPN (aha#). Please google for VPN if you don't know what it is...

Install it on both side.

The configuration file (@@/etc/openvpn/openvpn.conf@@) for the workstation (the client):

```
remote 100.100.100.100
dev tun
ping 30
ifconfig 10.4.16.2 10.4.16.1
proto tcp-client
port 443
```

The configuration file (@@/etc/openvpn/openvpn.conf@@) for the homestation (the server):

```
dev tun
ping 30
ifconfig 10.4.16.1 10.4.16.2
proto tcp-server
port 443
```

10.4.16.1 will be the IP address for homestation
10.4.16.2 will be the IP address for workstation

Here we are. Launch Openvpn on both sides.

```
# /etc/init.d/openvpn start
```

You should be able to ping 10.4.16.1 from your workstation (and to ping 10.4.16.2 from your homestation, as a side effect).

## Enable routing functions on homestation

On homestation, if you have a firewall, make it allow the connection from your workstation to the openvpn, make it allow your VPNed workstation to go out to the Internet. Allow forwarding.

Enable packet forwarding:

```
# sysctl -w net.ipv4.ip_forward=1
```

Don't forget to add this setting to @@/etc/sysctl.conf@@.

## Enable iproute2 routing on the workstation

To route firewalled packets through the VPN, we will use iptables to mark thoses packets using the POSTROUTING rule, then we will iproute2 to route marked packets through the VPN. Easy.

```
# we mark "1" packets that cannot go out to the Internet through the normal gateway
iptables -A POSTROUTING -t mangle -p tcp -m multiport --destination-ports # 80,443,8080 -j MARK --set-mark 1

# we create a new routing table with an ID not used yet (please check the file first)
echo "100 vpn" >> /etc/iproute2/rt_tables

# marked packets should use our new 'vpn' table
ip rule add fwmark 1 table vpn

# we route packets using the 'vpn' table through homestation
ip route add default via 10.4.16.1 table vpn
```

Et voilà. Now the workstation should access the Internet on every TCP ports.

## Bottom note

This article if a proof-of-concept in the simplest way. This is not secure at all. You're avised to read the manpage of every command/product before and the Advanced-routing-Howto.

If something doesn't work, ie.: "RTNETLINK answers: Invalid argument" you're probably missing some kernel modules.
