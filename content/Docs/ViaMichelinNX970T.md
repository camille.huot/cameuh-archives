---
title: "Hack your ViaMichelin"
date: 2012-03-27
---

Possesseur d'un GPS ViaMichelin Navigation X-970T, je souhaitais mettre à jour ma carte d'Europe quand j'ai découvert grâce aux résultats Google qu'on pouvait faire bien plus...

### Fin de support de Michelin

Premièrement, j'apprends que Michelin a arrêté la commercialisation de leur GPS, ainsi que la distribution des mises à jour des cartes : dernière mise à jour le 30 septembre 2008...

###Upgrade possible en version 7 de ViaMichelin Navigator

Ensuite, j'ai vu sur le site de Michelin qu'il existait plusieurs versions de mon logiciel de navigation. Apriori, je pense que je suis en version 6.5 car je vois des captures d'écran plus modernes qui appartiendraient à une version 7.

![Mon ViaMichelin en version 6.5]http://www.viamichelin.fr/tpl/psg/faq/img/support_version-0.jpg)

![Le ViaMichelin en version 7](http://www.viamichelin.fr/tpl/psg/faq/img/support_version-3.jpg)

J'aimerais bien avoir cette version 7.

###On peut mettre un autre logiciel

En cherchant comment migrer en version 7 (sur le site de Michelin, je trouve seulement un ''patch'' pour corriger un problème existant sur la version 7), certaines personnes parlent d'installer un autre logiciel, iGO8.

Pour changer le logiciel du GPS et le remplacer par iGO8, il suffirait de le copier sur la carte SD en lui donnant un nom bien particulier et de faire un reset du GPS. Au prochain reboot, le GPS installerait iGO8.

Extrait vu sur http://www.commentcamarche.net/forum/affich-6859802-mise-a-jour-viamichelin-x-970-t
->
```
carl39 - 17 déc. 2010 à 00:39
Bonjour,
Mais il y a bien plus intéressent ### une solution toute simple pour
faire une mise à jour cartographique 2009 ->sur un GPS viamichelin
960/970/980. Utilise iGO8, ça fonctionne bien.
Tu utilise ta carte sd 2Go
Dans le logiciel iGO8 tu vires touts les fichiers cartes qui ne servent à
rien du style usa,canada ...
pour gagner de la place.
Le dossier iGO tu le renomes InstallSD
Le fichier igo8.exe tu le renomes installsd.exe
Tu enleves ta carte SD du GPS
Tu effectues un hard reset sur ton GPS
Tu installes la nouvelle carte
Tu allumes le GPS et voila ça marche
A+
```

Très intéressant.

Je cherche alors différents mots clefs sur Google, viamichelin, tomtom, igo... et je trouve des personnes qui ont réussi à installer Tomtom Navigation sur leur GPS ViaMichelin.

Ah !

Je ne suis pas très expérimenté en la matière, mais j'ai constamment lu que le Tomtom était le meilleur et c'est ce que j'ai constaté lorsque j'en ai essayé un.

Donc cette possibilité m'intéresse.

###On peut accéder directement au Windows et installer plusieurs applications

Encore mieux # On le sait car c'est écrit derrière l'appareil, celui-ci tourne sous Windows CE Core 5.0. Et bien, l'installation du logiciel Vampirep permet de transformer l'appareil GPS en PDA, tout simplement.

![Le ViaMichelin 970T sous VampireP](http://outadoc.fr/wp-content/uploads/2011/04/TonioToulouse_4_VMTaskManager.jpg)

Lu sur http://outadoc.fr/2011/04/vampirep-auto-install-debrider-votre-viamichelin/

Cela permettrait visiblement de pouvoir installer plusieurs logiciels à la fois et de pouvoir choisir lequel utiliser à chaque fois. Mieux, j'imagine qu'on peut installer d'autres applications, du moment qu'elles sont compatibles avec Windows CE et qu'elles gèrent l'écran tactile.


### To be continued

En attendant des tests grandeur nature, les quelques liens supplémentaires qui me semblent intéressants :

* http://viamichelin-fr.custhelp.com/cgi-bin/viamichelin_fr.cfg/php/enduser/std_adp.php?p_faqid=2990&p_created=1197969953&p_sid=FYC
* http://www.navngo8.fr/igo_8_features.html
* http://www.mioskins.org/forum/viewtopic.php?f=49&t=19240&start=90
* http://www.gpspassion.com/forumsen/topic.asp?TOPIC_ID=107964
* http://www.filesdrop.com/free-tuto-installation-de-ttn7-sur-viamichelin-x-950/t51946/
