---
title: "School - 4-bit CPU with DesignWorks"
date: 2002-01-01
---
# Conception d'un microprocesseur 4 bits

Ce projet est le projet d'architecture réalisé en licence informatique au Département Informatique de Luminy à Marseille.

Brièvement, il s'agissait de refaire le fonctionnement d'un microprocesseur à l'aide du logiciel DesignWorks pour Windows. Dans la section "Téléchargement" il y a un fichier zip qui contient l'ensemble des fichiers qui ont été créés pour ce projet, ainsi que le rapport au format DocBookXML.

[[http://www.cameuh.net/files/projects/micro_4bits/rapport/|Voir le rapport]] (HTML)

[[http://www.cameuh.net/files/projects/micro_4bits/download/|Téléchargement]]
