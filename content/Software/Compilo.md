---
title: "School - Compilateur Voila"
date: 2002-01-01
---
# Réalisation d'un compilateur

Ce projet est le projet de théorie des langages et compilation réalisé en licence informatique au Département Informatique de Luminy à Marseille.

L'objet de ce projet est la réalisation d'un compilateur pour traduire des programmes écrits dans un langage à définir. Le code produit par le compilateur sera interprété par une machine virtuelle à pile.

Le compilateur est écrit en C et le langage source du compilateur est un mélange de C (pour la syntaxe, simplifiée) et de Pascal (pour la verbosité, francisée).

[Voir le rapport]
(/compilo/rapport/rapport.html)

[Télécharger les sources]
(/compilo/download/compilo.tar.bz2)
