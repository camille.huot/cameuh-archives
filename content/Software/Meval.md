---
title: "School - Méta-évaluateur Lisp en Lisp"
date: 2003-01-01
---
# Écriture d'un méta-évaluateur LISP en LISP

Ce projet est le projet de Langages, Évaluation, Compilation réalisé en maîtrise informatique au Département Informatique de Montpellier.

Le but était de reproduire le comportement de la fonction eval de LISP. Il a fallu simuler les fonctions primitives telles que if ou cond. Le méta-évaluateur peut s'évaluer lui-même, c'est à dire qu'on peut faire des choses du genre:

```
(meval '(meval '(factorielle 5)))
```

Pour information (pour les éventuels futurs maîtrises) le projet a obtenu 15. Donc faites mieux !

[[http://www.cameuh.net/files/projects/meval/|Téléchargement]]
