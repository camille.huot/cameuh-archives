---
title: "School - Peer to peer file sharing application"
date: 2002-01-01
---
# Réalisation d'une application peer-to-peer

Ce projet est le projet de Réseaux réalisé en maîtrise informatique au Département Informatique de Montpellier.

C'est un projet d'initiation à la programmation réseaux en socket. Le programme utilise de l'UDP pour la communication entre les différents agents, et du TCP pour les transferts de fichiers. Le serveur principal ainsi que les clients exportant des fichiers peuvent être "browsés" par un navigateur Internet.

{-[[http://www.cameuh.net/projects/mynap/download/|Téléchargement]]-}
