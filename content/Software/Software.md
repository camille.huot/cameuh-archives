---
title: "Software - Homepage"
date: 2007-04-10
---
Some other little scripts or applications are submitted to a Google Code Subversion repository, [[http://camtools.googlecode.com/svn/trunk/|camtools]] ([[http://code.google.com/p/camtools/|home page]]), without being listed here.

2010
* binck & libbinck, a Python command line client and library to access/order to the French stocks broker binck.fr.
2009
* [[http://camtools.googlecode.com/svn/trunk/multimedia/mkatag|mkatag]], a Python .mka tagger
* [[http://camtools.googlecode.com/svn/trunk/multimedia/mplayer-lastfm/|mkaplayer]], a Python Mplayer wrapper that reads tags from .mka files and send infos to Last.FM
2008
* [[http://camtools.googlecode.com/svn/trunk/python/cambank/|CamBank]], a python/cgi webapp to manage your personal bank account
2007
* [[http://camtools.googlecode.com/svn/trunk/multimedia/mplayer-lastfm/|Mplayer-LastFM]], a mplayer wrapper to send informations about what you played to Last.FM (Last.FM scrobbler for mplayer)
* [[http://camtools.googlecode.com/svn/trunk/administration/firewall/|Firewall]], a Bash frontend to iptables using OpenBSD pf style rules
2006
* [[http://developer.berlios.de/projects/rhngrab/|RhnGrab]], to fetch updated packages from Redhat Network
* check_os.sh, a full featured Bash script to monitor servers' updateness
2005
* OOoCam and Reporting, a Python lib and a tool to generate reporting directly using the OpenOffice.org API
2004
* [[KeymapWindows|Adaptation des dispositions de clavier franÃ§ais Xorg pour Windows]] [fr]
2003
* [[http://developer.berlios.de/projects/igoan/|Igoan]], a Freshmeat replacement
* Espere, an IRC robot trilogy: Perl, JAVA, Python
* [[http://www.nongnu.org/graphtool|Graphtool]], a tool for the creation and manipulation of any kind of graph, written in Python
* [[Meval|Ãcriture d'un mÃ©ta-Ã©valuateur LISP en LISP]] [fr]
2002
* [[MyNap|RÃ©alisation d'une application peer-to-peer]] [fr]
* [[Compilo|RÃ©alisation d'un compilateur]] [fr]
* [[CPU4bits|Conception d'un microprocesseur 4 bits]] [fr]
* [[Zoltrix|Windows driver for Zoltrix "Rainbow" 56K]]
