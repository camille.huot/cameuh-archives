---
title: "Driver - Modem Zoltrix Rainbow"
date: 2002-01-01
---
This is the Windows driver for the Zoltrix "Rainbow" (not sure) modem.

It is a fair good 56K modem, the problem is that he doesn't use the correct sequence to compose the phone number. You have to use ***ATP*T*** instead of ***ATDT*** to compose.

This driver allows you to use the modem as a normal modem with Windows. If you use AOL, you have to manually modify the compose sequence in the advanced configuration since AOL doesn't use the Windows drivers database.

{-[[http://www.cameuh.net/projects/zoltrix/download/|Download]]-}
